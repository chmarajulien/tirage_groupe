import csv
import random

# Lecture du fichier CSV
chemin_fichier_csv = 'promotion_B3_B.csv'

# Stockage des données dans une liste
donnees = []
with open(chemin_fichier_csv, 'r') as fichier:
    lecteur_csv = csv.reader(fichier, delimiter=';')
    next(lecteur_csv)  # On saute la première ligne
    for ligne in lecteur_csv:
            donnees.append({
                'nom': ligne[0],
                'prenom': ligne[1]
        })

# Mélanger les données de manière aléatoire
random.shuffle(donnees)

# Vérifier que le nombre d'entrées est pair
if len(donnees) % 2 != 0:
    print("Le nombre d'entrées dans le fichier CSV doit être pair pour former des groupes de deux.")
    exit()

# Diviser les données en groupes de deux
groupes = [donnees[i:i + 2] for i in range(0, len(donnees), 2)]

# Afficher les groupes avec les informations de chaque personne
for i, groupe in enumerate(groupes, start=1):
    personne1 = groupe[0]
    personne2 = groupe[1]
    print(f'Groupe {i}: {personne1["prenom"]} {personne1["nom"]} + {personne2["prenom"]} {personne2["nom"]}')
